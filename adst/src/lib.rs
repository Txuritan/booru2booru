use {
    flume::{Receiver, Sender, TryRecvError, TrySendError},
    futures_timer::Delay,
    std::{
        future::Future,
        io,
        pin::Pin,
        task::{Context, Poll},
        time::Duration,
    },
};

pub fn shutdown(
    dur: Duration,
) -> Result<(Heartbeat, Timer<impl Future<Output = io::Result<()>>>), Error> {
    let delay = Delay::new(dur);

    let (sender, receiver) = flume::bounded(1);

    let heartbeat = Heartbeat { sender };
    let timer = Timer {
        delay,
        signal: tokio::signal::ctrl_c(),

        dur,
        receiver,
    };

    Ok((heartbeat, timer))
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("Heartbeat channel has been disconnected, this is a bug")]
    Disconnected,
    #[error(transparent)]
    IO {
        #[from]
        err: io::Error,
    },
}

#[derive(Debug, Clone)]
pub struct Heartbeat {
    sender: Sender<()>,
}

impl Heartbeat {
    pub fn ping(&self) -> Result<(), Error> {
        tracing::debug!("sending heartbeat to timer");

        match self.sender.try_send(()) {
            Ok(_) => Ok(()),
            Err(TrySendError::Full(_)) => Ok(()),
            Err(TrySendError::Disconnected(_)) => Err(Error::Disconnected),
        }
    }
}

#[pin_project::pin_project]
pub struct Timer<S>
where
    S: Future<Output = io::Result<()>>,
{
    #[pin]
    delay: Delay,
    #[pin]
    signal: S,

    dur: Duration,
    receiver: Receiver<()>,
}

impl<S> Future for Timer<S>
where
    S: Future<Output = io::Result<()>>,
{
    type Output = Result<(), Error>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut proj = self.project();

        match proj.receiver.try_recv() {
            Ok(_) => {
                tracing::debug!("heartbeat received, resetting delay timer");

                let dur = *proj.dur;

                proj.delay.reset(dur);
            }
            Err(TryRecvError::Empty) => {}
            Err(TryRecvError::Disconnected) => {
                return Poll::Ready(Err(Error::Disconnected));
            }
        }

        if let Poll::Ready(()) = proj.delay.poll(cx) {
            tracing::info!("delay timer finished, shutting down");

            return Poll::Ready(Ok(()));
        }

        match proj.signal.poll(cx) {
            Poll::Ready(Ok(())) => {
                tracing::info!("signal received, shutting down");

                Poll::Ready(Ok(()))
            }
            Poll::Ready(Err(err)) => Poll::Ready(Err(Error::IO { err })),
            Poll::Pending => Poll::Pending,
        }
    }
}
