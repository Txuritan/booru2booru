use booru2booru_router::RouterBuilder;

pub trait D2RRouterExt {
    fn add_d2r(self) -> Self;
}

impl D2RRouterExt for RouterBuilder {
    fn add_d2r(self) -> Self {
        self
    }
}
