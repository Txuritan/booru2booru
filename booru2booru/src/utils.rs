use {
    booru2booru_router::Response, sentry::integrations::anyhow::capture_anyhow, std::future::Future,
};

#[inline(always)]
pub async fn wrap<Run, Fut>(run: Run) -> anyhow::Result<Response>
where
    Run: FnOnce() -> Fut,
    Fut: Future<Output = anyhow::Result<Response>>,
{
    match run().await {
        Ok(res) => Ok(res),
        Err(err) => {
            capture_anyhow(&err);

            {
                let span = tracing::error_span!("Response error");
                let _enter = span.enter();

                for chain in err.chain() {
                    tracing::error!("{}", chain);
                }
            }

            Ok(http::Response::builder()
                .status(503)
                .body("Internal Server Error".into())
                .unwrap())
        }
    }
}
