#![deny(rust_2018_idioms)]

mod assets;
mod d2e;
mod d2r;
mod utils;

use {
    crate::{assets::AssetsRouterExt, d2e::D2ERouterExt /* d2r::D2RRouterExt */},
    anyhow::Context,
    booru2booru_router::{Data, Request, RequestExtensions, Response, Router},
    dashmap::DashMap,
    hyper::Server,
    reqwest::Client,
    sentry_tracing::{SentryLayer, TracingIntegration, TracingIntegrationOptions},
    serde_json::Value,
    std::{
        env,
        net::{Ipv4Addr, SocketAddr, SocketAddrV4},
        sync::{
            atomic::{AtomicUsize, Ordering},
            Arc,
        },
    },
    tracing_subscriber::{fmt, layer::SubscriberExt as _, EnvFilter, Registry},
};

pub type Stats = Arc<AtomicUsize>;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let _sentry_guard = sentry::init((
        env::var("SENTRY_INIT").expect("No `SENTRY_INIT` in environment, this must exist"),
        sentry::apply_defaults({
            sentry::ClientOptions {
                environment: if cfg!(debug_assertions) {
                    Some("development".into())
                } else {
                    Some("production".into())
                },
                release: Some(
                    concat!(env!("CARGO_PKG_NAME"), "@", env!("CARGO_PKG_VERSION")).into(),
                ),
                ..Default::default()
            }
            .add_integration(TracingIntegration::new(TracingIntegrationOptions::default()))
        }),
    ));

    let reg = Registry::default()
        .with(fmt::Layer::new())
        .with(SentryLayer::default())
        .with(EnvFilter::from_default_env());

    tracing::subscriber::set_global_default(reg)?;

    if let Err(err) = run().await {
        sentry::integrations::anyhow::capture_anyhow(&err);

        {
            let span = tracing::error_span!("Error");
            let _enter = span.enter();

            for chain in err.chain() {
                tracing::error!("{}", chain);
            }
        }
    }

    Ok(())
}

#[allow(clippy::unit_arg)]
#[tracing::instrument(err)]
async fn run() -> anyhow::Result<()> {
    let port = env::args()
        .nth(1)
        .unwrap_or_else(|| env::var("PORT").unwrap_or_else(|_| "3000".to_string()))
        .parse::<u16>()
        .context("Unable to parse server port")?;

    let client = Client::builder()
        .gzip(true)
        .user_agent(concat!(
            env!("CARGO_PKG_NAME"),
            "/",
            env!("CARGO_PKG_VERSION"),
            " (by Txuritan on e621)"
        ))
        .build()?;

    let missing_routes: Arc<DashMap<String, String>> = Arc::new(DashMap::new());

    let statistics = Arc::new(AtomicUsize::new(0));

    let router = Router::build()
        .data(client)
        .wrap_data(Data::from_arc(missing_routes.clone()))
        .wrap_data(Data::from_arc(statistics.clone()))
        .get("/", index)
        .add_d2e()
        // .add_d2r()
        .add_assets()
        .not_found(not_found)
        .build();

    let addr = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), port));

    let server = Server::bind(&addr)
        .tcp_nodelay(true)
        .serve(router)
        .with_graceful_shutdown(async {
            tokio::signal::ctrl_c()
                .await
                .expect("failed to listen for event");
        });

    tracing::info!("booru2booru started on {}", addr);

    server.await?;

    sentry::with_scope(
        |scope| {
            let map = match Arc::try_unwrap(missing_routes) {
                Ok(map) => map
                    .into_iter()
                    .map(|(key, value)| (key, Value::from(value)))
                    .collect(),
                Err(map) => map
                    .iter()
                    .map(|pair| (pair.key().clone(), Value::from(pair.value().clone())))
                    .collect(),
            };

            scope.set_extra("missing_routes", Value::Object(map));
            scope.set_extra(
                "views",
                Value::Number(serde_json::Number::from(statistics.load(Ordering::SeqCst))),
            );
        },
        || {
            sentry::capture_message("booru2booru has shutdown", sentry::Level::Info);
        },
    );

    Ok(())
}

#[tracing::instrument(err, skip(req))]
async fn index(req: Request) -> anyhow::Result<Response> {
    utils::wrap(move || async move {
        if let Some(stats) = req.data::<Stats>() {
            let _ = stats.fetch_add(1, Ordering::SeqCst);
        }

        Ok(http::Response::builder()
            .status(200)
            .header(hyper::header::CONTENT_TYPE, "text/html; charset=utf-8")
            .body(include_str!("index.html").into())
            .unwrap())
    })
    .await
}

#[tracing::instrument(err, skip(req))]
async fn not_found(req: Request) -> anyhow::Result<Response> {
    utils::wrap(move || async move {
        if let Some(map) = req.data::<DashMap<String, String>>() {
            let uri = req.uri().path().to_string();

            if !map.contains_key(&uri) {
                map.insert(uri, req.uri().query().map(String::from).unwrap_or_default());
            }
        }

        if let Some(stats) = req.data::<Stats>() {
            let _ = stats.fetch_add(1, Ordering::SeqCst);
        }

        Ok(http::Response::builder().status(404).body("404".into())?)
    })
    .await
}
