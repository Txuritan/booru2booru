use {
    booru2booru_router::{Request, Response, RouterBuilder},
    hyper::body::Body,
};

macro_rules! handler {
    (png, $path:expr) => {{
        handler!("image/png", $path)
    }};
    ($mime:expr, $path:expr) => {{
        static CONTENT: &'static [u8] = include_bytes!($path);

        async fn inner(_req: Request) -> anyhow::Result<Response> {
            Result::<_, anyhow::Error>::Ok(
                http::Response::builder()
                    .header("Content-Type", $mime)
                    .body(Body::from(CONTENT))?,
            )
        }

        inner
    }};
}

pub trait AssetsRouterExt {
    fn add_assets(self) -> Self;
}

#[rustfmt::skip]
impl AssetsRouterExt for RouterBuilder {
    fn add_assets(self) -> Self {
        self
            .get("/android-chrome-144x144.png", handler!(png, "../assets/android-chrome-144x144.png"))
            .get("/android-chrome-192x192.png", handler!(png, "../assets/android-chrome-192x192.png"))
            .get("/android-chrome-256x256.png", handler!(png, "../assets/android-chrome-256x256.png"))
            .get("/android-chrome-36x36.png", handler!(png, "../assets/android-chrome-36x36.png"))
            .get("/android-chrome-384x384.png", handler!(png, "../assets/android-chrome-384x384.png"))
            .get("/android-chrome-48x48.png", handler!(png, "../assets/android-chrome-48x48.png"))
            .get("/android-chrome-512x512.png", handler!(png, "../assets/android-chrome-512x512.png"))
            .get("/android-chrome-72x72.png", handler!(png, "../assets/android-chrome-72x72.png"))
            .get("/android-chrome-96x96.png", handler!(png, "../assets/android-chrome-96x96.png"))
            .get("/apple-touch-icon-114x114-precomposed.png", handler!(png, "../assets/apple-touch-icon-114x114-precomposed.png"))
            .get("/apple-touch-icon-114x114.png", handler!(png, "../assets/apple-touch-icon-114x114.png"))
            .get("/apple-touch-icon-120x120-precomposed.png", handler!(png, "../assets/apple-touch-icon-120x120-precomposed.png"))
            .get("/apple-touch-icon-120x120.png", handler!(png, "../assets/apple-touch-icon-120x120.png"))
            .get("/apple-touch-icon-144x144-precomposed.png", handler!(png, "../assets/apple-touch-icon-144x144-precomposed.png"))
            .get("/apple-touch-icon-144x144.png", handler!(png, "../assets/apple-touch-icon-144x144.png"))
            .get("/apple-touch-icon-152x152-precomposed.png", handler!(png, "../assets/apple-touch-icon-152x152-precomposed.png"))
            .get("/apple-touch-icon-152x152.png", handler!(png, "../assets/apple-touch-icon-152x152.png"))
            .get("/apple-touch-icon-180x180-precomposed.png", handler!(png, "../assets/apple-touch-icon-180x180-precomposed.png"))
            .get("/apple-touch-icon-180x180.png", handler!(png, "../assets/apple-touch-icon-180x180.png"))
            .get("/apple-touch-icon-57x57-precomposed.png", handler!(png, "../assets/apple-touch-icon-57x57-precomposed.png"))
            .get("/apple-touch-icon-57x57.png", handler!(png, "../assets/apple-touch-icon-57x57.png"))
            .get("/apple-touch-icon-60x60-precomposed.png", handler!(png, "../assets/apple-touch-icon-60x60-precomposed.png"))
            .get("/apple-touch-icon-60x60.png", handler!(png, "../assets/apple-touch-icon-60x60.png"))
            .get("/apple-touch-icon-72x72-precomposed.png", handler!(png, "../assets/apple-touch-icon-72x72-precomposed.png"))
            .get("/apple-touch-icon-72x72.png", handler!(png, "../assets/apple-touch-icon-72x72.png"))
            .get("/apple-touch-icon-76x76-precomposed.png", handler!(png, "../assets/apple-touch-icon-76x76-precomposed.png"))
            .get("/apple-touch-icon-76x76.png", handler!(png, "../assets/apple-touch-icon-76x76.png"))
            .get("/apple-touch-icon-precomposed.png", handler!(png, "../assets/apple-touch-icon-precomposed.png"))
            .get("/apple-touch-icon.png", handler!(png, "../assets/apple-touch-icon.png"))
            .get("/browserconfig.xml", handler!("application/xml", "../assets/browserconfig.xml"))
            .get("/favicon-16x16.png", handler!(png, "../assets/favicon-16x16.png"))
            .get("/favicon-194x194.png", handler!(png, "../assets/favicon-194x194.png"))
            .get("/favicon-32x32.png", handler!(png, "../assets/favicon-32x32.png"))
            .get("/favicon.ico", handler!("image/x-icon", "../assets/favicon.ico"))
            .get("/mstile-144x144.png", handler!(png, "../assets/mstile-144x144.png"))
            .get("/mstile-150x150.png", handler!(png, "../assets/mstile-150x150.png"))
            .get("/mstile-310x150.png", handler!(png, "../assets/mstile-310x150.png"))
            .get("/mstile-310x310.png", handler!(png, "../assets/mstile-310x310.png"))
            .get("/mstile-70x70.png", handler!(png, "../assets/mstile-70x70.png"))
            .get("/safari-pinned-tab.svg", handler!("image/svg+xml", "../assets/safari-pinned-tab.svg"))
            .get("/site.webmanifest", handler!("application/manifest+json", "../assets/site.webmanifest"))
    }
}
