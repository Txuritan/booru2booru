use {
    crate::{utils, Stats},
    anyhow::Context,
    booru2booru_models::{danbooru2, e621, Convert},
    booru2booru_router::{Request, RequestExtensions, Response, RouterBuilder},
    codespan_reporting::{
        diagnostic::{Diagnostic, Label},
        files::SimpleFiles,
        term::{self, termcolor::NoColor, Config},
    },
    http::{
        uri::{self, Authority, PathAndQuery, Scheme, Uri},
        StatusCode,
    },
    hyper::{body::Bytes, Body},
    reqwest::{Client, Url},
    sentry::{Scope, User},
    serde_json::{Map, Number, Value},
    std::{convert::TryFrom, io::Write, iter::FromIterator as _, sync::atomic::Ordering},
    tracing_futures::Instrument,
};

pub trait D2ERouterExt {
    fn add_d2e(self) -> Self;
}

impl D2ERouterExt for RouterBuilder {
    fn add_d2e(self) -> Self {
        self.post(r#"/d2e/favorites"#, favorites_add)
            .delete(r#"/d2e/favorites/*"#, favorites_remove)
            .get(
                r#"/d2e/pools.json"#,
                convertible::<danbooru2::Pools, e621::Pools, Pools>,
            )
            .get(
                r#"/d2e/posts.json"#,
                convertible::<danbooru2::Posts, e621::Posts, Posts>,
            )
            .get(r#"/d2e/posts/*"#, post_get)
    }
}

#[tracing::instrument(err, skip(bytes))]
fn fallible_convert<T, F, K>(url: &Url, bytes: Bytes) -> anyhow::Result<Vec<u8>>
where
    F: Convert<T>,
    T: serde::Serialize,
    F: serde::de::DeserializeOwned,
    K: AsString,
{
    let json_de = &mut serde_json::Deserializer::from_slice(&bytes[..]);

    let json: F = match serde_path_to_error::deserialize(json_de) {
        Ok(json) => json,
        Err(err) => {
            let path = err.path().to_string();

            let inner_err = err.inner();

            let mut files = SimpleFiles::new();

            let text =
                std::str::from_utf8(&bytes[(inner_err.column() - 20)..(inner_err.column() + 20)])
                    .context("Unable to convert response bytes to UTF-8 string")?;

            let file_id = files.add("<json response>", text);

            let diagnostic = Diagnostic::error()
                .with_message("Unable to deserialize JSON response")
                .with_labels(vec![
                    Label::primary(file_id, 10..11).with_message(err.to_string())
                ]);

            let mut writer = NoColor::new(Vec::new());

            writeln!(&mut writer, "Error parsing JSON response")?;

            writeln!(&mut writer)?;

            term::emit(&mut writer, &Config::default(), &files, &diagnostic)
                .context("Unable to write codespan diagnostics information")?;

            let rendered = String::from_utf8(writer.into_inner())
                .context("Unable writer to UTF-8 string, this is a bug and should not happen")?;

            sentry::with_scope(
                |scope| {
                    set_user(scope, &url);
                    scope.set_extra("type", Value::String(K::as_string()));
                    scope.set_extra("json_path", Value::String(path));
                },
                || {
                    sentry::capture_message(&rendered, sentry::Level::Error);
                },
            );

            anyhow::bail!("{}", rendered);
        }
    };

    tracing::debug!("Converting JSON body to correct format");

    let mapped: T = json.convert();

    tracing::debug!("Writing converted body to buffer");

    let new_body = serde_json::to_vec(&mapped)?;

    tracing::debug!("Returning buffer");

    Ok(new_body)
}

trait AsString {
    fn as_string() -> String;
}

struct Post;

impl AsString for Post {
    fn as_string() -> String {
        String::from("post")
    }
}

struct Pools;

impl AsString for Pools {
    fn as_string() -> String {
        String::from("pools")
    }
}

struct Posts;

impl AsString for Posts {
    fn as_string() -> String {
        String::from("posts")
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct LoginQuery {
    pub login: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct TagQuery {
    pub tags: String,
}

#[tracing::instrument(err, skip(req))]
async fn favorites_add(mut req: Request) -> anyhow::Result<Response> {
    utils::wrap(move || async move {
        if let Some(stats) = req.data::<Stats>() {
            let _ = stats.fetch_add(1, Ordering::SeqCst);
        }

        tracing::debug!("Received request");

        let mut parts = req.uri_mut().clone().into_parts();

        convert_uri(&mut parts)?;

        let mut url = Url::parse(&Uri::from_parts(parts)?.to_string())?;

        let query = url.query().ok_or_else(|| {
            anyhow::anyhow!(
                "No query parameters in request, this is a problem on the application's side"
            )
        })?;

        tracing::debug!("Converting request query parameters");

        let (query, fav): (e621::Query, e621::FavoritePost) =
            serde_urlencoded::from_str::<'_, danbooru2::FavoritePost>(query)?.into();

        url.set_path("favorites.json");

        url.query_pairs_mut()
            .clear()
            .append_pair("login", &query.login)
            .append_pair("api_key", &query.api_key);

        tracing::debug!("Getting client from state");

        let client = req
            .data::<Client>()
            .ok_or_else(|| anyhow::anyhow!("No reqwest client found in router data"))?;

        tracing::debug!("Calling site API");

        let web_res: reqwest::Response = client
            .post(url.clone())
            .json(&fav)
            .send()
            .instrument(tracing::debug_span!("client"))
            .await?;

        let res = match web_res.status() {
            StatusCode::OK => http::Response::builder()
                .status(StatusCode::OK)
                .body(Body::empty())?,
            StatusCode::CREATED => http::Response::builder()
                .status(StatusCode::CREATED)
                .body(Body::empty())?,
            s => {
                let res_headers = web_res
                    .headers()
                    .iter()
                    .map(|(key, value)| {
                        value.to_str().map(|value| {
                            (key.as_str().to_string(), Value::String(value.to_string()))
                        })
                    })
                    .collect::<Result<Vec<_>, _>>()?;

                sentry::with_scope(
                    |scope| {
                        set_user(scope, &url);
                        scope.set_extra(
                            "body",
                            Value::Object({
                                let mut map = Map::new();

                                map.insert("post_id".into(), Value::String(fav.post_id.clone()));

                                map
                            }),
                        );
                        scope.set_extra(
                            "headers",
                            Value::Object(Map::from_iter(res_headers.into_iter())),
                        );
                        scope.set_extra(
                            "response_size",
                            web_res
                                .content_length()
                                .map(Number::from)
                                .map(Value::Number)
                                .unwrap_or_else(|| Value::Null),
                        );
                        scope.set_extra("path", Value::String(url.path().into()));
                        scope.set_extra("type", Value::String("favorites_add".into()));
                    },
                    || {
                        sentry::capture_message(
                            &format!("Website returned with status code: {}", s),
                            sentry::Level::Warning,
                        );
                    },
                );

                http::Response::builder().status(s).body(Body::empty())?
            }
        };

        tracing::info!(status = %res.status(), "finished");

        Ok(res)
    })
    .await
}

#[tracing::instrument(err, skip(req))]
async fn favorites_remove(mut req: Request) -> anyhow::Result<Response> {
    utils::wrap(move || async move {
        if let Some(stats) = req.data::<Stats>() {
            let _ = stats.fetch_add(1, Ordering::SeqCst);
        }

        tracing::debug!("Received request");

        let mut parts = req.uri_mut().clone().into_parts();

        convert_uri(&mut parts)?;

        let url = Url::parse(&Uri::from_parts(parts)?.to_string())?;

        tracing::debug!("Getting client from state");

        let client = req
            .data::<Client>()
            .ok_or_else(|| anyhow::anyhow!("No reqwest client found in router data"))?;

        tracing::debug!("Calling site API");

        let web_res: reqwest::Response = client
            .delete(url.clone())
            .send()
            .instrument(tracing::debug_span!("client"))
            .await?;

        let res = match web_res.status() {
            StatusCode::OK => http::Response::builder().status(200).body(Body::empty())?,
            s => {
                sentry::with_scope(
                    |scope| {
                        set_user(scope, &url);
                        scope.set_extra("path", Value::String(url.path().into()));
                        scope.set_extra("type", Value::String("favorites_remove".into()));
                    },
                    || {
                        sentry::capture_message(
                            &format!("Website returned with status code: {}", s),
                            sentry::Level::Warning,
                        );
                    },
                );

                http::Response::builder().status(s).body(Body::empty())?
            }
        };

        tracing::info!(status = %res.status(), "finished");

        Ok(res)
    })
    .await
}

#[tracing::instrument(err, skip(req))]
async fn convertible<T, F, K>(mut req: Request) -> anyhow::Result<Response>
where
    F: Convert<T>,
    T: serde::Serialize,
    F: serde::de::DeserializeOwned,
    K: AsString,
{
    utils::wrap(move || async move {
        if let Some(stats) = req.data::<Stats>() {
            let _ = stats.fetch_add(1, Ordering::SeqCst);
        }

        tracing::debug!("Received request");

        let url = {
            let mut parts = req.uri_mut().clone().into_parts();

            convert_uri(&mut parts)?;

            Url::parse(&Uri::from_parts(parts)?.to_string())?
        };

        tracing::debug!("Getting client from state");

        let client = req
            .data::<Client>()
            .ok_or_else(|| anyhow::anyhow!("No reqwest client found in router data"))?;

        tracing::debug!("Calling site API");

        let web_res: reqwest::Response = client
            .get(url.clone())
            .send()
            .instrument(tracing::debug_span!("client"))
            .await?;

        tracing::debug!("Checking status");

        let res = match web_res.status() {
            StatusCode::OK => {
                tracing::debug!("Status is OK, getting body as JSON");

                let bytes = web_res.bytes().await?;

                match fallible_convert::<T, F, K>(&url, bytes) {
                    Ok(new_body) => http::Response::builder()
                        .status(200)
                        .header("Content-Type", "application/json; charset=UTF-8")
                        .body(new_body.into())?,
                    Err(err) => {
                        tracing::error!("{}", err);

                        http::Response::builder()
                            .status(503)
                            .body("Internal Server Error".into())?
                    }
                }
            }
            s => {
                sentry::with_scope(
                    |scope| {
                        set_user(scope, &url);
                        scope.set_extra("path", Value::String(url.path().into()));
                        scope.set_extra(
                            "tag_query",
                            url.query()
                                .and_then(|query| {
                                    serde_urlencoded::from_str::<TagQuery>(query).ok()
                                })
                                .map(|query| Value::String(query.tags))
                                .unwrap_or_else(|| Value::Null),
                        );
                        scope.set_extra("type", Value::String(K::as_string()));
                    },
                    || {
                        sentry::capture_message(
                            &format!("Website returned with status code: {}", s),
                            sentry::Level::Warning,
                        );
                    },
                );

                http::Response::builder().status(s).body(Body::empty())?
            }
        };

        tracing::info!(status = %res.status(), "finished");

        Ok(res)
    })
    .await
}

#[tracing::instrument(err, skip(req))]
async fn post_get(mut req: Request) -> anyhow::Result<Response> {
    utils::wrap(move || async move {
        tracing::debug!("Received post request");

        let url = {
            let mut parts = req.uri_mut().clone().into_parts();

            convert_uri(&mut parts)?;

            Uri::from_parts(parts)?.to_string()
        };

        tracing::debug!("Redirecting to post");

        Ok(http::Response::builder()
            .status(301)
            .header("Location", url)
            .body(Body::empty())?)
    })
    .await
}

#[allow(clippy::unit_arg)]
#[tracing::instrument(level = "debug", err, skip(parts))]
fn convert_uri(parts: &mut uri::Parts) -> anyhow::Result<()> {
    parts.scheme = Some(Scheme::HTTPS);
    parts.authority = Some(Authority::from_static("e621.net"));
    parts.path_and_query = if let Some(paq) = parts.path_and_query.clone() {
        Some(PathAndQuery::try_from(
            paq.as_str().replace("/d2e/", "/").as_str(),
        )?)
    } else {
        None
    };

    Ok(())
}

#[inline]
fn set_user(scope: &mut Scope, url: &Url) {
    let username_or_email = url
        .query()
        .and_then(|query| serde_urlencoded::from_str::<'_, LoginQuery>(query).ok())
        .map(|login_query| login_query.login);
    let is_email = username_or_email
        .as_ref()
        .map(|either| either.contains('@'))
        .unwrap_or(false);

    scope.set_user(Some(if is_email {
        User {
            email: username_or_email,
            ..Default::default()
        }
    } else {
        User {
            username: username_or_email,
            ..Default::default()
        }
    }));
}
