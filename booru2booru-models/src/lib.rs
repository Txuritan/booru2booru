#![deny(rust_2018_idioms)]

pub mod danbooru2;
pub mod e621;

pub trait Convert<T> {
    fn convert(self) -> T;
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum Category {
    #[serde(rename = "collection")]
    Collection,
    #[serde(rename = "series")]
    Series,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum Rating {
    #[serde(rename = "e")]
    E,
    #[serde(rename = "q")]
    Q,
    #[serde(rename = "s")]
    S,
}
