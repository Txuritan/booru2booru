use crate::{e621, Category, Rating};

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct PostsQuery {
    pub login: Option<String>,
    pub api_key: Option<String>,
    pub limit: Option<u32>,
    pub tags: Option<String>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct FavoritePost {
    pub login: String,
    pub api_key: String,
    pub post_id: String,
}

impl Into<(e621::Query, e621::FavoritePost)> for FavoritePost {
    fn into(self) -> (e621::Query, e621::FavoritePost) {
        (
            e621::Query {
                login: self.login,
                api_key: self.api_key,
            },
            e621::FavoritePost {
                post_id: self.post_id,
            },
        )
    }
}

pub type Pools = Vec<Pool>;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Pool {
    pub id: i64,
    pub name: String,
    pub created_at: String,
    pub updated_at: Option<String>,
    pub category: Category,
    pub creator_id: Option<i64>,
    pub description: String,
    pub is_active: bool,
    pub is_deleted: bool,
    pub post_ids: Vec<i64>,
    pub creator_name: Option<String>,
    pub post_count: i64,
}

impl From<e621::Pool> for Pool {
    fn from(pool: e621::Pool) -> Self {
        Self {
            id: pool.id,
            name: pool.name,
            created_at: pool.created_at,
            updated_at: pool.updated_at,
            category: pool.category,
            creator_id: Some(pool.creator_id),
            description: pool.description,
            is_active: pool.is_active,
            is_deleted: pool.is_deleted,
            post_ids: pool.post_ids,
            creator_name: Some(pool.creator_name),
            post_count: pool.post_count,
        }
    }
}

pub type Posts = Vec<Post>;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Post {
    pub id: i64,
    pub created_at: String,
    pub updated_at: String,
    pub up_score: i64,
    pub down_score: i64,
    pub score: i64,
    pub source: String,
    pub md5: String,
    pub rating: Rating,
    pub is_note_locked: bool,
    pub is_rating_locked: bool,
    pub is_status_locked: bool,
    pub is_pending: bool,
    pub is_flagged: bool,
    pub is_deleted: bool,
    pub uploader_id: i64,
    pub approver_id: Option<i64>,
    pub pool_string: String,
    pub last_noted_at: Option<String>,
    pub last_comment_bumped_at: Option<String>,
    pub fav_count: i64,
    pub tag_string: String,
    pub tag_count: usize,
    pub tag_count_general: usize,
    pub tag_count_artist: usize,
    pub tag_count_character: usize,
    pub tag_count_copyright: usize,
    pub tag_count_meta: usize,
    pub file_ext: String,
    pub file_size: i64,
    pub image_width: i64,
    pub image_height: i64,
    pub parent_id: Option<i64>,
    pub has_children: bool,
    pub is_banned: bool,
    pub pixiv_id: Option<i64>,
    pub last_commented_at: Option<String>,
    pub has_active_children: bool,
    pub bit_flags: i64,
    pub has_large: bool,
    pub has_visible_children: bool,
    pub is_favorited: bool,
    pub tag_string_general: String,
    pub tag_string_character: String,
    pub tag_string_copyright: String,
    pub tag_string_artist: String,
    pub tag_string_meta: String,
    pub file_url: String,
    pub large_file_url: String,
    pub preview_file_url: Option<String>,
}

impl From<e621::Post> for Post {
    fn from(mut post: e621::Post) -> Self {
        if post.file.url.is_none() {
            tracing::info!(
                "URL for post {} doesn't exist, attempting to create it",
                post.id
            );

            post.file.url = Some(format!(
                "https://static1.e621.net/data/{}/{}/{}.{}",
                &post.file.md5[0..2],
                &post.file.md5[3..5],
                post.file.md5,
                post.file.ext
            ));
        }

        Self {
            id: post.id,
            created_at: post.created_at.clone(),
            updated_at: post.updated_at.unwrap_or(post.created_at),
            up_score: post.score.up,
            down_score: post.score.down,
            score: post.score.total,
            source: if let Some(source) = post.sources.get(0) {
                source.clone()
            } else {
                String::new()
            },
            md5: post.file.md5,
            rating: post.rating,
            file_ext: post.file.ext,
            file_size: post.file.size,
            preview_file_url: post.preview.url,
            large_file_url: post.file.url.clone().unwrap_or_else(String::new),
            file_url: post.file.url.unwrap_or_else(String::new),
            is_pending: post.flags.pending,
            is_flagged: post.flags.flagged,
            is_deleted: post.flags.deleted,
            is_status_locked: post.flags.status_locked.unwrap_or(false),
            is_note_locked: post.flags.note_locked,
            is_rating_locked: post.flags.rating_locked,
            image_height: post.file.height,
            image_width: post.file.width,
            parent_id: post.relationships.parent_id,
            has_children: post.relationships.has_children,
            has_active_children: post.relationships.has_active_children,
            approver_id: post.approver_id,
            uploader_id: post.uploader_id,
            fav_count: post.fav_count,
            is_favorited: post.is_favorited,
            tag_count: post.tags.general.len()
                + post.tags.species.len()
                + post.tags.character.len()
                + post.tags.copyright.len()
                + post.tags.artist.len()
                + post.tags.lore.len(),
            tag_count_general: post.tags.general.len()
                + post.tags.species.len()
                + post.tags.lore.len(),
            tag_count_artist: post.tags.artist.len(),
            tag_count_character: post.tags.character.len(),
            tag_count_copyright: post.tags.copyright.len(),
            tag_count_meta: post.tags.meta.len(),
            tag_string: {
                let mut vec = Vec::with_capacity(
                    post.tags.general.len()
                        + post.tags.species.len()
                        + post.tags.lore.len()
                        + post.tags.artist.len()
                        + post.tags.character.len()
                        + post.tags.copyright.len()
                        + post.tags.meta.len(),
                );

                vec.append(&mut (post.tags.artist.clone()));
                vec.append(&mut (post.tags.character.clone()));
                vec.append(&mut (post.tags.copyright.clone()));
                vec.append(&mut (post.tags.species.clone()));
                vec.append(&mut (post.tags.lore.clone()));
                vec.append(&mut (post.tags.meta.clone()));
                vec.append(&mut (post.tags.general.clone()));

                vec.join(" ")
            },
            tag_string_general: {
                let mut vec = Vec::with_capacity(
                    post.tags.general.len() + post.tags.species.len() + post.tags.lore.len(),
                );

                vec.append(&mut (post.tags.lore));
                vec.append(&mut (post.tags.species));
                vec.append(&mut (post.tags.general));

                vec.join(" ")
            },
            tag_string_artist: post.tags.artist.join(" "),
            tag_string_character: post.tags.character.join(" "),
            tag_string_copyright: post.tags.copyright.join(" "),
            tag_string_meta: post.tags.meta.join(" "),

            // TODO: figure these out
            has_visible_children: false,
            is_banned: false,
            has_large: true,
            pixiv_id: None,
            last_noted_at: None,
            last_commented_at: None,
            last_comment_bumped_at: None,
            pool_string: String::new(),
            bit_flags: 2,
        }
    }
}

impl From<e621::Posts> for Posts {
    fn from(posts: e621::Posts) -> Posts {
        posts.posts.into_iter().map(|p| p.into()).collect()
    }
}
