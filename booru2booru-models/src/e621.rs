use crate::{danbooru2, Category, Convert, Rating};

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Query {
    pub login: String,
    pub api_key: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct FavoritePost {
    pub post_id: String,
}

pub type Pools = Vec<Pool>;

impl Convert<danbooru2::Pools> for Pools {
    fn convert(self) -> danbooru2::Pools {
        self.into_iter().map(|p| p.into()).collect()
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Pool {
    pub id: i64,
    pub name: String,
    pub created_at: String,
    pub updated_at: Option<String>,
    pub category: Category,
    pub creator_id: i64,
    pub description: String,
    pub is_active: bool,
    pub is_deleted: bool,
    pub post_ids: Vec<i64>,
    pub creator_name: String,
    pub post_count: i64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Posts {
    pub posts: Vec<Post>,
}

impl Convert<danbooru2::Posts> for Posts {
    fn convert(self) -> danbooru2::Posts {
        self.posts.into_iter().map(|p| p.into()).collect()
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Post {
    pub id: i64,
    pub created_at: String,
    pub updated_at: Option<String>,
    pub file: File,
    pub preview: Preview,
    pub sample: Sample,
    pub score: Score,
    pub tags: Tags,
    pub locked_tags: Vec<String>,
    pub change_seq: i64,
    pub flags: Flags,
    pub rating: Rating,
    pub fav_count: i64,
    pub sources: Vec<String>,
    pub pools: Vec<i64>,
    pub relationships: Relationships,
    pub approver_id: Option<i64>,
    pub uploader_id: i64,
    pub description: String,
    pub comment_count: i64,
    pub is_favorited: bool,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct File {
    pub width: i64,
    pub height: i64,
    pub ext: String,
    pub size: i64,
    pub md5: String,
    pub url: Option<String>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Flags {
    pub pending: bool,
    pub flagged: bool,
    pub note_locked: bool,
    pub status_locked: Option<bool>,
    pub rating_locked: bool,
    pub deleted: bool,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Preview {
    pub width: i64,
    pub height: i64,
    pub url: Option<String>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Sample {
    pub has: Option<bool>,
    pub height: i64,
    pub width: i64,
    pub url: Option<String>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Relationships {
    pub parent_id: Option<i64>,
    pub has_children: bool,
    pub has_active_children: bool,
    pub children: Vec<i64>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Score {
    pub up: i64,
    pub down: i64,
    pub total: i64,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Tags {
    pub general: Vec<String>,
    pub species: Vec<String>,
    pub character: Vec<String>,
    pub copyright: Vec<String>,
    pub artist: Vec<String>,
    pub invalid: Vec<String>,
    pub lore: Vec<String>,
    pub meta: Vec<String>,
}
