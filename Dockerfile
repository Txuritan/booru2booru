# Used to build the app and not in final.
FROM ekidd/rust-musl-builder:1.48.0 AS build

WORKDIR /home/rust/src

# Add our source code.
COPY --chown=rust:rust . ./

# Build our final application.
RUN cargo build --release



# Buld the final package.
FROM alpine:latest

RUN apk --no-cache add ca-certificates

# Copy our built application into the new container
COPY --from=build /home/rust/src/target/x86_64-unknown-linux-musl/release/booru2booru /usr/local/bin/

# Run as non-root user.
RUN adduser -D myuser
USER myuser

# Run our application.
CMD /usr/local/bin/booru2booru $PORT
