use {std::sync::Arc, type_map::concurrent::TypeMap};

pub struct Data<T> {
    pub(crate) inner: Arc<T>,
}

impl<T> Data<T> {
    pub fn new(inner: T) -> Self {
        Data {
            inner: Arc::new(inner),
        }
    }

    pub fn from_arc(inner: Arc<T>) -> Self {
        Data { inner }
    }
}

impl<T> std::ops::Deref for Data<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &*self.inner
    }
}

impl<T> Clone for Data<T> {
    fn clone(&self) -> Self {
        Data {
            inner: self.inner.clone(),
        }
    }
}

#[derive(Clone)]
pub struct DataMap {
    pub inner: Arc<TypeMap>,
}
