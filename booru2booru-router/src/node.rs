pub enum NodeKind {
    Static(String),
    Named,
    Wildcard,
}

pub struct Node<T> {
    pub(crate) kind: NodeKind,
    pub(crate) data: Option<T>,

    pub(crate) indices: Option<String>,
    pub(crate) nodes: Option<Vec<Self>>,
    pub(crate) params: Option<Vec<String>>,
}

impl<T> Node<T> {
    pub(crate) fn new(kind: NodeKind) -> Self {
        Self {
            kind,
            data: None,
            params: None,
            indices: None,
            nodes: None,
        }
    }

    pub(crate) fn add_node(&mut self, c: char, kind: NodeKind) -> &mut Self {
        let indices: &mut String = self.indices.get_or_insert_with(String::new);
        let nodes: &mut Vec<Node<T>> = self.nodes.get_or_insert_with(Vec::new);

        match indices.chars().position(|x| x == c) {
            Some(i) => match kind {
                NodeKind::Static(ref s) => nodes[i].insert(s),
                _ => &mut nodes[i],
            },
            None => {
                indices.push(c);
                nodes.push(Node::new(kind));
                nodes.last_mut().unwrap()
            }
        }
    }

    pub(crate) fn add_node_static(&mut self, p: &str) -> &mut Self {
        if let Some(c) = p.chars().next() {
            self.add_node(c, NodeKind::Static(p.to_owned()))
        } else {
            self
        }
    }

    pub(crate) fn add_node_dynamic(&mut self, c: char, kind: NodeKind) -> &mut Self {
        self.add_node(c, kind)
    }

    pub(crate) fn insert(&mut self, path: &str) -> &mut Self {
        match self.kind {
            NodeKind::Static(ref mut static_path) if static_path.is_empty() => {
                *static_path += path;

                self
            }
            NodeKind::Static(ref mut static_path) => {
                let np: String = loc(static_path, path);
                let l = np.len();

                if l < static_path.len() {
                    *static_path = static_path[l..].to_owned();

                    let mut node = Node {
                        data: None,
                        params: None,
                        nodes: Some(Vec::new()),
                        indices: static_path.chars().next().map(|c| c.to_string()),
                        kind: NodeKind::Static(np),
                    };

                    std::mem::swap(self, &mut node);

                    self.nodes.as_mut().unwrap().push(node);
                }

                if l == path.len() {
                    self
                } else {
                    self.add_node_static(&path[l..])
                }
            }
            NodeKind::Named => self.add_node_static(path),
            NodeKind::Wildcard => self,
        }
    }

    #[tracing::instrument(level = "trace", skip(self))]
    pub(crate) fn find<'a>(&'a self, mut p: &'a str) -> Option<(&'a Self, Vec<&'a str>)> {
        let mut params = Vec::new();

        match self.kind {
            NodeKind::Static(ref s) => {
                let np = loc(s, p);
                let l = np.len();

                if l == 0 || l < s.len() {
                    None
                } else if l == s.len() && l == p.len() {
                    Some((
                        // Fixed: has only route `/*`
                        // Ended `/` `/*any`
                        if self.data.is_none() && self.indices.is_some() && s.ends_with('/') {
                            &self.nodes.as_ref().unwrap()
                                [position(self.indices.as_ref().unwrap(), '*')?]
                        } else {
                            self
                        },
                        params,
                    ))
                } else {
                    let indices = self.indices.as_ref()?;
                    let nodes = self.nodes.as_ref()?;

                    p = &p[l..];

                    // Static
                    if let Some(i) = position(indices, p.chars().next().unwrap()) {
                        if let Some((n, ps)) = nodes[i].find(p).as_mut() {
                            params.append(ps);

                            return Some((
                                // Ended `/` `/*any`
                                match &n.kind {
                                    NodeKind::Static(s)
                                        if n.data.is_none()
                                            && n.indices.is_some()
                                            && s.ends_with('/') =>
                                    {
                                        &n.nodes.as_ref().unwrap()
                                            [position(n.indices.as_ref().unwrap(), '*')?]
                                    }
                                    _ => n,
                                },
                                params,
                            ));
                        }
                    }

                    // Named Parameter
                    if let Some(i) = position(indices, ':') {
                        if let Some((n, ps)) = nodes[i].find(p).as_mut() {
                            params.append(ps);

                            return Some((n, params));
                        }
                    }

                    // Wildcard Parameter
                    if let Some(i) = position(indices, '*') {
                        if let Some((n, ps)) = nodes[i].find(p).as_mut() {
                            params.append(ps);

                            return Some((n, params));
                        }
                    }

                    None
                }
            }
            NodeKind::Named => match position(p, '/') {
                Some(i) => {
                    let indices = self.indices.as_ref()?;

                    params.push(&p[..i]);
                    p = &p[i..];

                    let (n, ref mut ps) = self.nodes.as_ref().unwrap()
                        [position(indices, p.chars().next().unwrap())?]
                    .find(p)?;

                    params.append(ps);

                    Some((n, params))
                }
                None => {
                    params.push(p);

                    Some((self, params))
                }
            },
            NodeKind::Wildcard => {
                params.push(p);

                Some((self, params))
            }
        }
    }
}

#[inline]
fn position(p: &str, c: char) -> Option<usize> {
    p.chars().position(|x| x == c)
}

#[inline]
fn loc(s: &str, p: &str) -> String {
    s.chars()
        .zip(p.chars())
        .take_while(|(a, b)| a == b)
        .map(|v| v.0)
        .collect()
}
