#![deny(rust_2018_idioms)]

mod data;
mod node;
mod param;
mod route;
mod tree;

use hyper::Body;

pub use self::{data::Data, param::Params, route::BoxedAsyncRoute};

pub type Request = hyper::Request<Body>;
pub type Response = hyper::Response<Body>;

pub trait RequestExtensions {
    fn param(&self, key: String) -> Option<String>;

    fn wildcard(&self) -> Option<String>;

    fn data<T: Send + Sync + 'static>(&self) -> Option<Data<T>>;
}

impl RequestExtensions for Request {
    fn param(&self, key: String) -> Option<String> {
        self.extensions()
            .get::<param::Params>()
            .and_then(|data| data.params.as_ref().and_then(|params| params.get(&key)))
            .cloned()
    }

    fn wildcard(&self) -> Option<String> {
        self.extensions()
            .get::<param::Params>()
            .and_then(|data| data.wildcard.as_ref())
            .cloned()
    }

    fn data<T: Send + Sync + 'static>(&self) -> Option<Data<T>> {
        self.extensions()
            .get::<data::DataMap>()
            .and_then(|data| data.inner.get::<Data<T>>())
            .cloned()
    }
}

use {
    crate::{data::DataMap, tree::Tree},
    futures_util::future::{ready, Ready},
    hyper::server::conn::AddrStream,
    std::{
        collections::{BTreeMap, HashMap},
        future::Future,
        pin::Pin,
        sync::Arc,
        task::{Context, Poll},
    },
    tower_service::Service,
    type_map::concurrent::TypeMap,
};

pub struct RouterBuilder {
    data: Option<TypeMap>,
    not_found: Option<BoxedAsyncRoute>,
    method_handlers: HashMap<http::Method, Tree<BoxedAsyncRoute>>,
}

impl RouterBuilder {
    pub fn data<T: Send + Sync + 'static>(self, data: T) -> Self {
        self.wrap_data(Data::new(data))
    }

    pub fn wrap_data<T: Send + Sync + 'static>(mut self, data: Data<T>) -> Self {
        let mut map = self.data.take().unwrap_or_else(TypeMap::new);

        map.insert(data);

        self.data = Some(map);

        self
    }

    pub fn not_found(mut self, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.not_found = Some(handler.into());

        self
    }

    pub fn add(
        mut self,
        method: http::Method,
        path: impl AsRef<str>,
        handler: impl Into<BoxedAsyncRoute>,
    ) -> Self {
        let path = path.as_ref();
        let handler = handler.into();

        let tree = self.method_handlers.entry(method).or_insert_with(Tree::new);

        tree.insert(path, handler);

        self
    }

    pub fn get(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::GET, path, handler)
    }

    pub fn post(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::POST, path, handler)
    }

    pub fn put(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::PUT, path, handler)
    }

    pub fn delete(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::DELETE, path, handler)
    }

    pub fn head(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::HEAD, path, handler)
    }

    pub fn options(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::OPTIONS, path, handler)
    }

    pub fn connect(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::CONNECT, path, handler)
    }

    pub fn patch(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::PATCH, path, handler)
    }

    pub fn trace(self, path: impl AsRef<str>, handler: impl Into<BoxedAsyncRoute>) -> Self {
        self.add(http::Method::TRACE, path, handler)
    }

    pub fn build(self) -> Router {
        let mut inner = RouterInner {
            data: self.data.map(|inner| DataMap {
                inner: Arc::new(inner),
            }),
            not_found: self
                .not_found
                .unwrap_or_else(|| BoxedAsyncRoute::from(Self::default_not_found)),
            method_handlers: MethodMap::new(),
        };

        for (method, tree) in self.method_handlers {
            inner.method_handlers.insert(method, tree);
        }

        Router {
            inner: Arc::new(inner),
        }
    }

    async fn default_not_found(_: Request) -> anyhow::Result<Response> {
        Ok(http::Response::builder()
            .status(404)
            .body("Not Found".into())
            .unwrap())
    }
}

#[derive(Clone)]
pub struct Router {
    inner: Arc<RouterInner>,
}

impl Router {
    pub fn build() -> RouterBuilder {
        RouterBuilder {
            data: None,
            not_found: None,
            method_handlers: HashMap::new(),
        }
    }
}

impl Service<Request> for Router {
    type Response = Response;
    type Error = anyhow::Error;
    type Future = Pin<Box<dyn Future<Output = anyhow::Result<Self::Response>> + Send>>;

    fn poll_ready(&mut self, _: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    #[tracing::instrument(level = "trace", name = "router::call", skip(self, req))]
    fn call(&mut self, mut req: Request) -> Self::Future {
        ::tracing::info!(method = %req.method(), path = %req.uri().path(), query = ?req.uri().query(), "request");

        let service = Arc::clone(&self.inner);

        if let Some(data) = self.inner.data.clone() {
            let ext_mut = req.extensions_mut();

            ext_mut.insert(data);
        }

        if let Some(router) = service.method_handlers.get(req.method()) {
            if let Some((handler, params)) = router.find(req.uri().path()) {
                let handler = handler.inner.clone();

                let mut mapped_params = None;
                let mut mapped_wildcard = None;

                for (k, v) in params {
                    if k.is_empty() {
                        mapped_wildcard = Some(v.to_string());
                    } else {
                        mapped_params
                            .get_or_insert_with(BTreeMap::new)
                            .insert(k.to_string(), v.to_string());
                    }
                }

                let params = Params {
                    params: mapped_params.map(Arc::new),
                    wildcard: mapped_wildcard,
                };

                let ext_mut = req.extensions_mut();

                ext_mut.insert(params);

                return handler(req);
            }
        }

        let handler = service.not_found.inner.clone();

        handler(req)
    }
}

impl<'a> Service<&'a AddrStream> for Router {
    type Response = Router;
    type Error = Box<dyn std::error::Error + Send + Sync + 'static>;
    type Future = Ready<Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, _: &'a AddrStream) -> Self::Future {
        ready(Ok(Router {
            inner: Arc::clone(&self.inner),
        }))
    }
}

struct RouterInner {
    data: Option<DataMap>,
    not_found: BoxedAsyncRoute,
    method_handlers: MethodMap,
}

struct MethodMap {
    get: Option<Tree<BoxedAsyncRoute>>,
    post: Option<Tree<BoxedAsyncRoute>>,
    put: Option<Tree<BoxedAsyncRoute>>,
    delete: Option<Tree<BoxedAsyncRoute>>,
    head: Option<Tree<BoxedAsyncRoute>>,
    options: Option<Tree<BoxedAsyncRoute>>,
    connect: Option<Tree<BoxedAsyncRoute>>,
    patch: Option<Tree<BoxedAsyncRoute>>,
    trace: Option<Tree<BoxedAsyncRoute>>,
    extension: Option<HashMap<http::Method, Tree<BoxedAsyncRoute>>>,
}

impl MethodMap {
    fn new() -> Self {
        Self {
            get: None,
            post: None,
            put: None,
            delete: None,
            head: None,
            options: None,
            connect: None,
            patch: None,
            trace: None,
            extension: None,
        }
    }

    fn insert(&mut self, method: http::Method, tree: Tree<BoxedAsyncRoute>) {
        match method {
            http::Method::GET => {
                self.get = Some(tree);
            }
            http::Method::POST => {
                self.post = Some(tree);
            }
            http::Method::PUT => {
                self.put = Some(tree);
            }
            http::Method::DELETE => {
                self.delete = Some(tree);
            }
            http::Method::HEAD => {
                self.head = Some(tree);
            }
            http::Method::OPTIONS => {
                self.options = Some(tree);
            }
            http::Method::CONNECT => {
                self.connect = Some(tree);
            }
            http::Method::PATCH => {
                self.patch = Some(tree);
            }
            http::Method::TRACE => {
                self.trace = Some(tree);
            }
            m => {
                let mut extension = self.extension.take().unwrap_or_else(HashMap::new);
                extension.insert(m, tree);
                self.extension = Some(extension);
            }
        }
    }

    fn get(&self, method: &http::Method) -> Option<&Tree<BoxedAsyncRoute>> {
        match *method {
            http::Method::GET => self.get.as_ref(),
            http::Method::POST => self.post.as_ref(),
            http::Method::PUT => self.put.as_ref(),
            http::Method::DELETE => self.delete.as_ref(),
            http::Method::HEAD => self.head.as_ref(),
            http::Method::OPTIONS => self.options.as_ref(),
            http::Method::CONNECT => self.connect.as_ref(),
            http::Method::PATCH => self.patch.as_ref(),
            http::Method::TRACE => self.trace.as_ref(),
            ref m => self.extension.as_ref().and_then(|e| e.get(m)),
        }
    }
}
