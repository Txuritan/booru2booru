use {
    crate::{Request, Response},
    futures_core::TryFuture,
    std::{
        future::Future,
        pin::Pin,
        sync::Arc,
        task::{Context, Poll},
    },
};

pub trait AsyncRoute {
    type Future: Future<Output = anyhow::Result<Response>> + Send;

    fn call(&self, request: Request) -> Self::Future;
}

impl<H, F> AsyncRoute for H
where
    H: Fn(Request) -> F,
    F: Future<Output = anyhow::Result<Response>> + Send,
{
    type Future = AsyncHandler<F>;

    fn call(&self, request: Request) -> Self::Future {
        AsyncHandler {
            inner: self(request),
        }
    }
}

#[pin_project::pin_project]
pub struct AsyncHandler<F> {
    #[pin]
    inner: F,
}

impl<F> Future for AsyncHandler<F>
where
    F: Future<Output = anyhow::Result<Response>> + Send,
{
    type Output = anyhow::Result<Response>;

    #[inline]
    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let poll = match self.project().inner.try_poll(ctx) {
            Poll::Ready(t) => t,
            Poll::Pending => return Poll::Pending,
        };

        Poll::Ready(poll)
    }
}

pub struct BoxedAsyncRoute {
    #[allow(clippy::type_complexity)]
    pub(crate) inner: Arc<
        dyn Fn(Request) -> Pin<Box<dyn Future<Output = anyhow::Result<Response>> + Send>>
            + Send
            + Sync,
    >,
}

impl BoxedAsyncRoute {
    fn new<A: AsyncRoute + Send + Sync + 'static>(route: A) -> Self {
        Self {
            inner: Arc::new(move |req: Request| Box::pin(route.call(req))),
        }
    }
}

impl<H, F> From<H> for BoxedAsyncRoute
where
    H: Fn(Request) -> F + Send + Sync + 'static,
    F: Future<Output = anyhow::Result<Response>> + Send,
{
    fn from(route: H) -> BoxedAsyncRoute {
        BoxedAsyncRoute::new(route)
    }
}
