use super::node::{Node, NodeKind};

pub struct Tree<T> {
    root: Node<T>,
}

impl<T> Tree<T> {
    pub fn new() -> Self {
        Self {
            root: Node::new(NodeKind::Static("/".to_owned())),
        }
    }

    pub fn insert(&mut self, mut path: &str, data: impl Into<T>) -> &mut Self {
        let data = data.into();
        let mut next = true;
        let mut node = &mut self.root;
        let mut params: Option<Vec<String>> = None;

        path = path.trim_start_matches('/');

        if path.is_empty() {
            node.data.replace(data);

            return self;
        }

        while next {
            match path.chars().position(has_colon_or_star) {
                Some(i) => {
                    let kind: NodeKind;
                    let mut prefix = &path[..i];
                    let mut suffix = &path[i..];

                    if !prefix.is_empty() {
                        node = node.add_node_static(prefix);
                    }

                    prefix = &suffix[..1];
                    suffix = &suffix[1..];

                    let c = prefix.chars().next().unwrap();
                    if c == ':' {
                        match suffix.chars().position(has_star_or_slash) {
                            Some(i) => {
                                path = &suffix[i..];
                                suffix = &suffix[..i];
                            }
                            None => {
                                next = false;
                            }
                        }
                        kind = NodeKind::Named;
                    } else {
                        next = false;
                        kind = NodeKind::Wildcard;
                    }
                    params.get_or_insert_with(Vec::new).push(suffix.to_owned());
                    node = node.add_node_dynamic(c, kind);
                }
                None => {
                    next = false;
                    node = node.add_node_static(path);
                }
            }
        }

        node.data = Some(data);

        node.params = params;

        self
    }

    #[tracing::instrument(level = "trace", skip(self))]
    pub fn find<'a>(&'a self, path: &'a str) -> Option<(&'a T, Vec<(&'a str, &'a str)>)> {
        let span = tracing::trace_span!("find-route");
        let _entered = span.enter();

        self.root.find(path).and_then(|(node, values)| {
            node.data.as_ref().map(|data| {
                (
                    data,
                    node.params.as_ref().map_or_else(Vec::new, |params| {
                        params
                            .iter()
                            .zip(values.iter())
                            .map(|(a, b)| (a.as_str(), *b))
                            .collect()
                    }),
                )
            })
        })
    }
}

#[inline]
fn has_colon_or_star(c: char) -> bool {
    (c == ':') | (c == '*')
}

#[inline]
fn has_star_or_slash(c: char) -> bool {
    (c == '*') | (c == '/')
}
