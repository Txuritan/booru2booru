use std::{collections::BTreeMap, sync::Arc};

#[derive(Clone)]
pub struct Params {
    pub(crate) params: Option<Arc<BTreeMap<String, String>>>,
    pub(crate) wildcard: Option<String>,
}
