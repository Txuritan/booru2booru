# booru2booru

booru2booru is a web based request transformer, as in it takes a request to one booru site and return it in the style of another.

## Usage

To use booru2booru, add `{this website URL}/{transform postfix}` to a booru browser server listing with the correct server type.

Please note that there should be **NO** double slashes (`//`) in the URL.

See below for available transforms.

## Transforms

| Postfix | Source | Server Type |
| ------- | ------ | ----------- |
| `/d2e/` | e621   | Danbooru 2  |

## Building

To build and run booru2booru you'll need a C compile and Rust installed.

Then just run `cargo build --release`.
